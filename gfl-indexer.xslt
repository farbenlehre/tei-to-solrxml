<?xml version="1.0" encoding="UTF-8"?>

<!-- 

This script produces two types of Solr XML documents, representing letters and individual pages of those letters.

The letter documents contain many metadata fields and two text fields. The metadata fields are
values that are copied from the TEI files. The text fields are described next.

Field 'fulltext'

This is just plain text copied from the complete TEI file. Line breaks and page beginnings are
transformed to whitespace. This field can be useful for searching or for generating highlighting snippets. 

Field 'fulltext_html'

This field  contains the HTML representation of the text of a TEI file (e. g. a Goethe letter).
A letter that consists of several pages is split on-the-fly into those pages.
However, the pages are all kept in this one field. The templates that are used here are the same
as the ones for the individual page documents (see below).

The letters are composed of different parts, for example 'opener', 'closer', 'salute'.
All those parts are represented here as <div>'s with the corresponding CSS classes.
The frontend viewer must decide how to format those parts and present them to the user.

The original TEI files contain mark-up for many in-text parts, like dates, names, underlined words, etc.
Most of these are also transformed to <div>'s with their own CSS classes.
Although the in-text parts are by nature inline elements, we use here <div>'s and not <span>'s.
The reason is that Solr seems to have problems when highlighting fields that contain <span>'s
by sometimes producing corrupt HTML.
By using <div>'s, we avoid this problem.
In the frontend, these <div>'s must be set to 'display: inline'.

Some other in-text parts are transformed to special HTML elements.
For example, superscripted text is marked as <sup>, because HTML offers the appropriate element.

The project is still continuing and new TEI files are being produced.
That's why there might be new elements in the future that cannot be handled yet in this script.
The text of such TEI elements is enclosed in HTML elements of class 'unknown-element'.
Furthermore, a warning message is generated that contains data of the first occurrence of such a new element.


The second kind of documents that are produced are page documents.
The resulting pages are in the HTML format.
As the TEI file is processed, the TEI XML structure is split into pages using 
the page beginning elements (<pb/>).
Refer to comments in the code to understand the used algorithm.

-->

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xpath-default-namespace="http://www.tei-c.org/ns/1.0" xmlns:gfl="http://sub.gfl.de"
   xmlns:xs="http://www.w3.org/2001/XMLSchema" 
   xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="gfl saxon xs"
   xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
   xmlns:marc="http://www.loc.gov/MARC21/slim"
   xmlns:img="http://whatever">

   <xsl:output method="xml" indent="yes" saxon:suppress-indentation="div" />
   <xsl:strip-space elements="*" />
   <xsl:preserve-space elements="msIdentifier bibl p" />

   <xsl:template match="/">
      <xsl:apply-templates select="TEI" />
   </xsl:template>

   <xsl:template match="TEI">
      <add>
         <doc>    
            <xsl:apply-templates select="teiHeader|text" />
            <xsl:for-each select="distinct-values(//@xml:lang)">
               <field name="language">
                  <xsl:value-of select="." />
               </field>
            </xsl:for-each>
            
            <xsl:apply-templates select="facsimile" />
            <xsl:for-each select="distinct-values(//name/@ref)">
               <xsl:if test=". != 'gnd:_' and starts-with(., 'gnd:') ">
               <field name="entities">
                  <xsl:value-of select="."/>
               </field>
               </xsl:if>
            </xsl:for-each>
            <xsl:for-each select="//text//note/@type">
               <xsl:variable name="generate_id" select="count(preceding::note) + 1" />
               <field name="notes">
                     <xsl:value-of select="//text/@xml:id" />
                     <xsl:text>_note_</xsl:text>
                     <xsl:value-of select="$generate_id" />
               </field>               
               
            </xsl:for-each>
         </doc>
         <xsl:apply-templates select="//body/listBibl" /> 
         <xsl:apply-templates select="//text//name/@ref"/>
         <xsl:apply-templates select="//text//note/@type"/>
         <xsl:apply-templates select="text" mode="page_splitting" />
      </add>
   </xsl:template>
   
   <xsl:template match="//text//note/@type">
      
      <doc>
         <xsl:variable name="generateid" select="count(preceding::note) + 1" />
         <field name="id">
            <xsl:value-of select="//text/@xml:id" />
            <xsl:text>_note_</xsl:text>
            <xsl:value-of select="$generateid" />
         </field>
         <field name="doctype">note</field>
         <field name="note">
            <xsl:value-of select="./parent::note/text()/normalize-space()"/> 
         </field>
         <field name="note_type">
            <xsl:value-of select="."/> 
         </field>
         <field name="note_place">
            <xsl:value-of select="./parent::note/@place"/> 
         </field>   
      </doc>
   </xsl:template>
   
   <xsl:template match="name/@ref">
      <xsl:if test=". != 'gnd:_' and ./parent::name/text() and ./parent::name/@type and starts-with(., 'gnd:') ">
         <doc>
            <field name="entity_name">
               <xsl:value-of select="./parent::name/normalize-space()"/> 
            </field>    
            <field name ="doctype">
               <xsl:value-of select="./parent::name/@type/normalize-space()"/> 
             </field>
               <!-- person -> Person
                    place -> Geografikum
                    org -> Organisation
                    object -> Sachbegriff-->
                  <!--xsl:choose>
                     <xsl:when test="./parent::name/@type = 'person'">
                        <xsl:text>Person</xsl:text>
                     </xsl:when>
                     <xsl:when test="./parent::name/@type = 'place'">
                        <xsl:text>Geografikum</xsl:text>
                     </xsl:when>
                     <xsl:when test="./parent::name/@type = 'org'">
                        <xsl:text>Organisation</xsl:text>
                     </xsl:when>
                     <xsl:when test="./parent::name/@type = 'object'">
                        <xsl:text>Sachbegriff</xsl:text>
                     </xsl:when>
                     <xsl:otherwise>
                        <xsl:value-of select="./parent::name/@type"/>
                     </xsl:otherwise>
                  </xsl:choose>
                 </field-->        
           
            <field name="id">  
               <xsl:value-of select="."/>
            </field>
            <xsl:variable name="gnd_number">
               <xsl:value-of select="normalize-space(substring-after(., 'gnd:'))"/>
            </xsl:variable>
            <xsl:variable name="uri_path">
               <xsl:value-of select="concat('https://d-nb.info/gnd/', $gnd_number, '/about/marcxml')"/>
            </xsl:variable>
            <xsl:choose>
               <xsl:when test="doc-available($uri_path)">
                  <xsl:apply-templates select="doc($uri_path)/marc:record" />
               </xsl:when>
            </xsl:choose>
         </doc>
      </xsl:if>
   </xsl:template>
   
   <!-- scraping the gnd database for meta data the result is marc21 with works with numbers as identifyer -->
   <xsl:template match="marc:record">
      <xsl:choose>
         <xsl:when test="marc:datafield[@tag='100']">
            <field name="mostly_use_name">  
               <xsl:value-of  select="marc:datafield[@tag='100']/marc:subfield[@code='a']"/>
               <xsl:if test="marc:datafield[@tag='100']/marc:subfield[@code='g']">
                  <xsl:text> </xsl:text>
                  <xsl:value-of  select="marc:datafield[@tag='100']/marc:subfield[@code='g']"/>
               </xsl:if>
            </field>
         </xsl:when>
         <xsl:when test="marc:datafield[@tag='110']">
            <field name="mostly_use_name">  
               <xsl:value-of  select="marc:datafield[@tag='110']/marc:subfield[@code='a']"/>
               <xsl:if test="marc:datafield[@tag='110']/marc:subfield[@code='g']">
                  <xsl:text> </xsl:text>
                  <xsl:value-of  select="marc:datafield[@tag='110']/marc:subfield[@code='g']"/>
               </xsl:if>
            </field>
         </xsl:when>
         <xsl:when test="marc:datafield[@tag='111']">
            <field name="mostly_use_name">  
               <xsl:value-of  select="marc:datafield[@tag='111']/marc:subfield[@code='a']"/>
               <xsl:if test="marc:datafield[@tag='111']/marc:subfield[@code='g']">
                  <xsl:text> </xsl:text>
                  <xsl:value-of  select="marc:datafield[@tag='111']/marc:subfield[@code='g']"/>
               </xsl:if>
            </field>
         </xsl:when>
         <xsl:when test="marc:datafield[@tag='130']">
            <field name="mostly_use_name">  
               <xsl:value-of  select="marc:datafield[@tag='130']/marc:subfield[@code='a']"/>
               <xsl:if test="marc:datafield[@tag='130']/marc:subfield[@code='g']">
                  <xsl:text> </xsl:text>
                  <xsl:value-of  select="marc:datafield[@tag='130']/marc:subfield[@code='g']"/>
               </xsl:if>
            </field>
         </xsl:when>
         <xsl:when test="marc:datafield[@tag='150']">
            <field name="mostly_use_name">  
               <xsl:value-of  select="marc:datafield[@tag='150']/marc:subfield[@code='a']"/>
               <xsl:if test="marc:datafield[@tag='150']/marc:subfield[@code='g']">
                  <xsl:text> </xsl:text>
                  <xsl:value-of  select="marc:datafield[@tag='150']/marc:subfield[@code='g']"/>
               </xsl:if>
            </field>
         </xsl:when>
         <xsl:when test="marc:datafield[@tag='151']">
            <field name="mostly_use_name">  
               <xsl:value-of  select="marc:datafield[@tag='151']/marc:subfield[@code='a']"/>
               <xsl:if test="marc:datafield[@tag='151']/marc:subfield[@code='g']">
                  <xsl:text> </xsl:text>
                  <xsl:value-of  select="marc:datafield[@tag='151']/marc:subfield[@code='g']"/>
               </xsl:if>
            </field>
         </xsl:when>
      </xsl:choose> 
      <xsl:for-each select="marc:datafield[@tag='400']">
      <field name="alternatively_name">  
         <xsl:value-of select="marc:subfield[@code='a']"/>
         <xsl:choose>
         <xsl:when test="marc:subfield[@code='c']">
            <xsl:text> </xsl:text>   
            <xsl:value-of select="marc:subfield[@code='c']"/>
         </xsl:when>
         </xsl:choose>
      </field>
      </xsl:for-each>
      <xsl:for-each select="marc:datafield[@tag='410']">
         <field name="alternatively_name">  
            <xsl:value-of select="marc:subfield[@code='a']"/>
            <xsl:choose>
               <xsl:when test="marc:subfield[@code='b']">
                  <xsl:text> </xsl:text>   
                  <xsl:value-of select="marc:subfield[@code='b']"/>
               </xsl:when>
               <xsl:when test="marc:subfield[@code='g']">
                  <xsl:text> </xsl:text>   
                  <xsl:value-of select="marc:subfield[@code='g']"/>
               </xsl:when>
            </xsl:choose>
         </field>
      </xsl:for-each>
      <xsl:for-each select="marc:datafield[@tag='430']">
         <field name="alternatively_name">  
            <xsl:value-of select="marc:subfield[@code='a']"/><xsl:text></xsl:text>   
            <xsl:value-of select="marc:subfield[@code='g']"/><xsl:text></xsl:text>   
            <xsl:value-of select="marc:subfield[@code='n']"/>
         </field>
      </xsl:for-each>
      <xsl:for-each select="marc:datafield[@tag='451']">
         <field name="alternatively_name">  
            <xsl:value-of select="marc:subfield[@code='a']"/>
         </field>
      </xsl:for-each>
      <xsl:for-each select="marc:datafield[@tag='034']"> 
         <field name="west_cordinate">  
            <xsl:value-of select="marc:subfield[@code='e']"/>
         </field>
         <field name="east_cordinate">  
            <xsl:value-of select="marc:subfield[@code='e']"/>
         </field>
         <field name="north_cordinate">  
            <xsl:value-of select="marc:subfield[@code='f']"/>
         </field>
         <field name="south_cordinate">  
            <xsl:value-of select="marc:subfield[@code='g']"/>
         </field>
      </xsl:for-each>
   </xsl:template>
   
   <!--Doctype Literature scraped from body/listBibl for each bibl tag in body;  -->
   <xsl:template match="//body/listBibl">
         <xsl:for-each select="bibl">
            <doc>
               <field name="id"> 
                  <xsl:value-of select="replace(./@xml:id , '_', ' ')"/>
               </field>
               <field name="doctype">literature</field>
                  <xsl:for-each select="relatedItem">
                    <xsl:choose>
                       <xsl:when test="ref/@target = 'https://_' or ref/@target = '' or ref/@target = '_'">
                          <field name="uri"/>
                       </xsl:when>
                       <xsl:otherwise>
                          <field name="uri">
                             <xsl:value-of select="ref/@target"/>
                          </field>
                       </xsl:otherwise>
                    </xsl:choose>         
                  </xsl:for-each>     
               <xsl:for-each select="author">
                  <field name="literature_author"> 
                     <xsl:value-of select="./normalize-space()"/>
                  </field>
               </xsl:for-each>
               <xsl:for-each select="editor">
                  <field name="editor"> 
                     <xsl:value-of select="./normalize-space()"/>
                  </field>
               </xsl:for-each>
               <!-- TITLE -->
                  <xsl:for-each select="title">
                     <xsl:choose>
                        <xsl:when test="./@type = 'main'">
                           <xsl:choose>
                              <xsl:when test="./@level = 'a'">
                                 <field name="analytic_main_title"> 
                                    <xsl:value-of select="./normalize-space()"/>
                                 </field>
                              </xsl:when>
                              <xsl:when test="./@level = 'm'">
                                 <field name="monographic_main_title"> 
                                    <xsl:value-of select="./normalize-space()"/>
                                 </field>
                              </xsl:when>
                              <xsl:when test="./@level = 'j'">
                                 <field name="journal_main_title"> 
                                    <xsl:value-of select="./normalize-space()"/>
                                 </field>
                              </xsl:when>
                              <xsl:when test="./@level = 's'">
                                 <field name="series_main_title"> 
                                    <xsl:value-of select="./normalize-space()"/>
                                 </field>
                              </xsl:when>
                              <xsl:when test="./@level = 'u'">
                                 <field name="unpublished_main_title"> 
                                    <xsl:value-of select="./normalize-space()"/>
                                 </field>
                              </xsl:when>
                           </xsl:choose>   
                        </xsl:when>
                        <xsl:when test="./@type = 'sub'">
                           <xsl:choose>
                              <xsl:when test="./@level = 'a'">
                                 <field name="analytic_sub_title"> 
                                    <xsl:value-of select="./normalize-space()"/>
                                 </field>
                              </xsl:when>
                              <xsl:when test="./@level = 'm'">
                                 <field name="monographic_sub_title"> 
                                    <xsl:value-of select="./normalize-space()"/>
                                 </field>
                              </xsl:when>
                              <xsl:when test="./@level = 'j'">
                                 <field name="journal_sub_title"> 
                                    <xsl:value-of select="./normalize-space()"/>
                                 </field>
                              </xsl:when>
                              <xsl:when test="./@level = 's'">
                                 <field name="series_sub_title"> 
                                    <xsl:value-of select="./normalize-space()"/>
                                 </field>
                              </xsl:when>
                              <xsl:when test="./@level = 'u'">
                                 <field name="unpublished_sub_title"> 
                                    <xsl:value-of select="./normalize-space()"/>
                                 </field>
                              </xsl:when>
                           </xsl:choose>   
                        </xsl:when>
                        <xsl:otherwise>
                           <field name="title"> 
                              <xsl:value-of select="./normalize-space()"/>
                           </field>
                        </xsl:otherwise>
                     </xsl:choose>
                  </xsl:for-each>
                   <xsl:for-each select="biblScope">
                      <xsl:if test=".[@unit='volume'and @n] ">
                         <field name="biblScope_volume_n"> 
                            <xsl:value-of select="./normalize-space()"/>
                         </field>
                      </xsl:if>
                      <xsl:if test=".[@unit='volume'] [not(@n)] ">
                        <field name="biblScope_volume"> 
                           <xsl:value-of select="./normalize-space()"/>
                        </field>
                      </xsl:if>
                      <xsl:if test=".[@unit='pages']">
                         <field name="biblScope_pages"> 
                            <xsl:value-of select="./normalize-space()"/>
                         </field>
                      </xsl:if>  
                </xsl:for-each>  
               <xsl:if test="edition!=''">
                  <field name="edition"> 
                     <xsl:value-of select="edition/normalize-space()"/>
                  </field>
               </xsl:if> 
               <xsl:for-each select="pubPlace/name">
               <field name="pub_place"> 
                  <xsl:value-of select="./normalize-space()"/>
               </field>
               </xsl:for-each>
               <xsl:if test="extent">
                  <field name="extent"> 
                     <xsl:value-of select="extent/normalize-space()"/>
                  </field>
               </xsl:if>
               <xsl:if test="publisher">
                  <field name="publisher">
                     <xsl:value-of select="publisher/normalize-space()"/>
                  </field>               
               </xsl:if>
               <xsl:if test="date/@type='pub'">
                  <field name="lit_pub_date">
                     <xsl:value-of select="date/normalize-space()"/>
                  </field>
               </xsl:if>
               <xsl:if test="idno !='' and idno[@type = 'ISBN']">
                     <field name="ISBN"> 
                        <xsl:value-of select="idno/normalize-space()"/>
                     </field>   
               </xsl:if>
               <xsl:if test="idno !='' and idno[@type = 'ISSN']">
                  <field name="ISSN"> 
                     <xsl:value-of select="idno/normalize-space()"/>
                  </field>   
               </xsl:if>   
           </doc>
           </xsl:for-each>
   </xsl:template>
   

   <xsl:template match="text()" mode="text_only">
      <xsl:variable name="currentText" select="replace(., '\s+', ' ')" />
      <xsl:choose>
         <xsl:when test="ends-with(., '-')">
            <!-- These are cases where one word is divided between two lines. 
            The minus sign is removed here, the line break (<lb/>) is removed in its own template. -->
            <xsl:value-of select="substring($currentText, 1, string-length($currentText)-1)" />
         </xsl:when>
         <xsl:when test="ends-with(., '&#0173;')">
            <!-- A soft hyphen is a convention to mark a hyphen that belongs to the word or is a hyphen on its own.
            For now, it is just replaced by a minus sign. 
            Later, it might be useful to differentiate between word divisions and hyphens. -->
            <xsl:value-of select="replace($currentText, '&#0173;', '-')" />
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$currentText" />
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   
   <!--###########   Header   #######################-->
   
   <xsl:template match="teiHeader">
      <xsl:apply-templates select="fileDesc | profileDesc/textClass" />
   </xsl:template>
   
   <xsl:template match="fileDesc">
      <xsl:apply-templates select="titleStmt/title" />
      <xsl:apply-templates select="titleStmt/title[@type='desc']/name" />
      <xsl:apply-templates select="titleStmt/title[@type='desc']/date[@type='orn']" />
      <xsl:apply-templates select="titleStmt/author/name" />
      <xsl:apply-templates select="sourceDesc" />
      <xsl:apply-templates select="publicationStmt/availability/licence" />
   </xsl:template>
   
   <xsl:template match="profileDesc/textClass">
      <xsl:apply-templates select="keywords/term" />
   </xsl:template>
   
   <xsl:template match="title[@type='short']">
      <field name="short_title">
         <xsl:apply-templates mode="text_only" />
      </field>
   </xsl:template>

   <xsl:template match="title[@type='desc']">
      <field name="title">
         <xsl:apply-templates mode="text_only" />
      </field>
   </xsl:template>

   <xsl:template match="title/name[@type='place' and @subtype='orn']">
      <field name="origin_place">
         <xsl:apply-templates mode="text_only" />
      </field>
   </xsl:template>

   <xsl:template match="title/name[@type='place' and @subtype='dtn']">
      <field name="destination_place">
         <xsl:apply-templates mode="text_only" />
      </field>
   </xsl:template>

   <xsl:template match="title/name[@type='person' and @subtype='rcp']">
      <field name="recipient">
         <xsl:apply-templates mode="text_only" />
      </field>
   </xsl:template>

   <xsl:template match="title/date[@type='orn']">
            <xsl:if test="@when">
            <field name="origin_date">
               <xsl:value-of select="@when" />
            </field>
               <field name="from">
                  <xsl:value-of select="@when" />
               </field>
               <field name="to">
                  <xsl:value-of select="@when" />
               </field>
            </xsl:if>
            <xsl:if test="@from and @to ">
               <field name="from">
                  <xsl:value-of select="@from" />
               </field>
               <field name="to">
                  <xsl:value-of select="@to" />
               </field>
            </xsl:if>
            <xsl:if test="@notAfter">
               <field name="from">
                  <xsl:value-of select="@notAfter" />
               </field>
               <field name="to">
                  <xsl:value-of select="@notAfter" />
               </field>
            </xsl:if>
            
      <field name="article_pub_date">
         <xsl:value-of select="./text()/normalize-space()" />
      </field>
   </xsl:template>

   <xsl:template match="author/name">
      <field name="author">
         <xsl:apply-templates mode="text_only" />
      </field>
   </xsl:template>
   
   <xsl:template match="sourceDesc">
      <field name="source_description">
         <xsl:apply-templates mode="text_only" />
      </field>
   </xsl:template>

   <xsl:template match="licence">
      <field name="license">
         <xsl:value-of select="text()" />
      </field>
   </xsl:template>

   <xsl:template match="textClass/keywords[@scheme='#gnd']/term">
      <field name="gnd_keyword">
         <xsl:apply-templates mode="text_only" />
      </field>
   </xsl:template>

   <xsl:template match="textClass/keywords[@scheme='free']/term">
      <field name="free_keyword">
         <xsl:apply-templates mode="text_only" />
      </field>
   </xsl:template>
   
   <xsl:template match="textClass/keywords[@scheme='frei']/term">
      <field name="free_keyword">
         <xsl:apply-templates mode="text_only" />
      </field>
   </xsl:template>

   <!--###################   text/body   ##########################-->  

   <xsl:template match="facsimile">
      <xsl:for-each select="./graphic/@xml:id">
         <field name="image_ids"> 
            <xsl:value-of select="."/>
         </field>
      </xsl:for-each>    
      <xsl:for-each select="./graphic/@url">
         <field name="image_urls">
            <xsl:value-of select="."/>
         </field> 
      </xsl:for-each>    
   </xsl:template> 

   <xsl:template match="text">
      <xsl:choose>
         <xsl:when test="./@xml:id"> 
            <field name="id">
               <xsl:value-of select="./@xml:id"/>
            </field> 
         </xsl:when>
         <xsl:when test="contains(document-uri(/), 'Lit_Archiv/')"> <!-- TODO delte workaround for ids -->
            <field name="id">
               <xsl:value-of select="substring-after(substring-before(document-uri(/), '.xml'), 'Lit_Archiv/')" />
            </field> 
         </xsl:when>
         <xsl:when test="contains(document-uri(/), 'Lit_Fbl_Bln/')"> 
            <field name="id">
               <xsl:value-of select="substring-after(substring-before(document-uri(/), '.xml'), 'Lit_Fbl_Bln/')" />
            </field> 
         </xsl:when>
         <xsl:when test="contains(document-uri(/), 'Lit_Bearbeitung/')"> 
            <field name="id">
               <xsl:value-of select="substring-after(substring-before(document-uri(/), '.xml'), 'Lit_Bearbeitung/')" />
            </field>
         </xsl:when>
         <xsl:otherwise>
            <field name="id">
               <xsl:value-of select="document-uri(/)" /> 
            </field> 
         </xsl:otherwise>
      </xsl:choose>

      <xsl:choose> 
         <xsl:when test="contains(document-uri(/), 'Lit')"> 
            <field name="doctype">literature_meta</field>
         </xsl:when>

         <xsl:otherwise>
            <field name="doctype">article</field><!--TODO  test!!! -->
            <field name="number_of_pages">
               <xsl:value-of select="count(//pb)" />
            </field>
            <field name="fulltext">
               <xsl:apply-templates select="body" mode="text_only" />
            </field>
            <field name="fulltext_html">
               <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
               <div class="article">
                  <xsl:variable name="context" select="." />
                  <xsl:for-each-group select="descendant::node()[not(node())]" group-starting-with="pb">
                     <xsl:if test="self::pb">
                        <div class="page">
                           <xsl:call-template name="page-beginning-with-possible-link">
                              <xsl:with-param name="current-pb" select="." />
                           </xsl:call-template>
                           <xsl:apply-templates select="$context/*" mode="page_splitting">
                              <xsl:with-param name="restricted-to" select="current-group()/ancestor-or-self::node()" tunnel="yes" />
                           </xsl:apply-templates>
                        </div>
                     </xsl:if>
                  </xsl:for-each-group>
               </div>
               <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
            </field>
            <field name="institution">
               <xsl:if test="//repository != '' ">
                  <xsl:value-of select="//repository/normalize-space()"/><xsl:text>, </xsl:text>
               </xsl:if>
               <xsl:if test="//institution != '' ">
                  <xsl:value-of select="//institution/normalize-space()"/><xsl:text>, </xsl:text>
               </xsl:if>
               <xsl:if test="//settlement != '' ">
                  <xsl:value-of select="//settlement/normalize-space()"/>
               </xsl:if>
               <xsl:if test="//country != '' ">
                  <xsl:text> (</xsl:text>
                  <xsl:value-of select="//country/normalize-space()"/><xsl:text>)</xsl:text>
               </xsl:if>
            </field>
            <field name="shelfmark">
               <xsl:value-of select="//idno/normalize-space()"/>
            </field>
            <field name="script_source">
               <xsl:if test="//supportDesc/extent/text() != '' ">
                  <xsl:value-of select="//supportDesc/extent/text()"/>
                  <xsl:text> </xsl:text>
               </xsl:if>
               <xsl:if test="//supportDesc/extent/dimensions/height != ''">
                  <xsl:value-of select="//supportDesc/extent/dimensions/height/normalize-space()"/><xsl:text> x </xsl:text>
               </xsl:if>
               <xsl:if test="//supportDesc/extent/dimensions/width != ''">
                  <xsl:value-of select="//supportDesc/extent/dimensions/width/normalize-space()"/><xsl:text> </xsl:text>
               </xsl:if>
               <xsl:if test="//supportDesc/extent/dimensions/@unit">
                  <xsl:value-of select="//supportDesc/extent/dimensions/@unit/normalize-space()"/><xsl:text>. </xsl:text>
               </xsl:if>
               <xsl:if test="//bindingDesc != ''">
                  <xsl:value-of select="//bindingDesc/normalize-space()"/>
               </xsl:if>
            </field>
            <xsl:for-each select="//handNote">
            <field name="writer">
               <xsl:value-of select="./normalize-space()"/><xsl:if test="@scope='major'"> – (Grundschicht)</xsl:if>
            </field>
            </xsl:for-each>
            <field name="reference">
               <xsl:value-of select="//relatedItem[@subtype = 'related']/normalize-space()"/>
            </field>
            <field name="response">
               <xsl:value-of select="//relatedItem[@subtype = 'response']/normalize-space()"/>
            </field>
            <xsl:for-each select="//relatedItem[@type = 'letter' and not(@subtype)]">
               <field name="related_items">
                  <xsl:value-of select="./ref/normalize-space()"/>
               </field>
            </xsl:for-each>
         </xsl:otherwise>
       </xsl:choose>

      <xsl:apply-templates select=".//note[@type='com']" />
     
   </xsl:template>
   
   <xsl:template match="body | div" mode="text_only">
      <xsl:apply-templates mode="text_only"/>
   </xsl:template>

   <xsl:template match="p | salute | signed  | dateline" mode="text_only"> <!--addrLine -->
      <xsl:apply-templates mode="text_only" />
      <xsl:text> </xsl:text>
   </xsl:template>

   <xsl:template match="note[@place='end' and not(@type='com')]" mode="text_only">
      <xsl:text> </xsl:text>
      <xsl:apply-templates mode="text_only" />
   </xsl:template>

   <xsl:template match="lb" mode="text_only">
      <xsl:variable name="precedingText" select="preceding-sibling::text()[1]" />
      <xsl:choose>
         <xsl:when test="ends-with($precedingText, '-')">
            <!-- Cases where a word is divided between two lines -->
            <!-- no output -->
         </xsl:when>
         <xsl:when test="ends-with($precedingText, '&#0173;') and not(ends-with($precedingText, ' &#0173;'))">
            <!-- Cases where the hyphen belongs to the word (Anna-<lb/>Lena) -->
            <!-- no output -->
         </xsl:when>
         <xsl:otherwise>
            <xsl:text> </xsl:text>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <xsl:template match="pb" mode="text_only">
      <xsl:text> </xsl:text>
   </xsl:template>

   <xsl:template match="space" mode="text_only">
      <xsl:text> </xsl:text>
   </xsl:template>

   <xsl:template match="note[@type='com']" mode="text_only">
   </xsl:template>
   
   <xsl:template match="note[@type='com']">
      <field name="note_comment">
         <xsl:apply-templates mode="text_only" />
      </field>
   </xsl:template>
   
   
   <!-- %%%%%%%%%%%%% page splitting and HTML generating %%%%%%%%%%%%%%%%%%%%%%% -->
   <!-- returns the image path for page-->
   <xsl:function name="img:get_path" as="xs:string">
      <xsl:param name="image_id"/>
      <xsl:param name="context"/>
      <xsl:value-of select="$context/preceding-sibling::facsimile/graphic[@xml:id=$image_id]/@url"/>
   </xsl:function>

   <!-- Here we start the page splitting algorithm. 
      In general, it groups all elements between two <pb/>'s and creates one page for each such group.
      Note: We use a trick with a tunnel parameter. The templates that are run from here must check 
      if they really should execute or not using that tunnel parameter. -->
   <xsl:template match="text" mode="page_splitting">
      <!-- We keep the parent of all pages (-> super-parent). -->
      <xsl:variable name="context" select="." />
      <!-- We make groups of only 'small' nodes, like text and <pb/>.
         We need this granularity, because a <pb/> can separate two text nodes to different pages. -->
      <xsl:for-each-group select="descendant::node()[not(node())]" group-starting-with="pb">
         <xsl:if test="self::pb">
            <doc>
               <xsl:variable name="pageNumber" select="count(preceding::pb) + 1" />
            
               <field name="id">
                  <xsl:value-of select="$context/@xml:id" />
                  <xsl:text>_page</xsl:text>
                  <xsl:value-of select="$pageNumber" />
               </field>
               <field name="article_id">
                  <xsl:value-of select="$context/@xml:id" />
               </field>
               <field name="article_title">
                  <xsl:value-of select="//title[@type='desc']/normalize-space()" />
               </field>
               <field name="doctype">page</field>
               <field name="page_number">
                  <xsl:value-of select="$pageNumber" />
               </field>
               <field name="html_page">
                  <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
                  <div class="page">
                     <xsl:call-template name="page-beginning-with-possible-link">
                        <xsl:with-param name="current-pb" select="." />
                     </xsl:call-template>
                     <!-- We run the templates of all the super-parent's children... -->
                     <xsl:apply-templates select="$context/*" mode="page_splitting">
                        <!-- ..., but we use a trick with a tunnel parameter to restrict the actually executed templates 
                           to the ones relevant for the current page. 
                           We choose the grouped nodes, as well as all their ancestors.
                           We need all the ancestors to construct correctly opened and closed elements 
                           that otherwise would be cut in two by the <pb/>. -->
                        <xsl:with-param name="restricted-to" select="current-group()/ancestor-or-self::node()" tunnel="yes" />
                     </xsl:apply-templates>
                  </div>
                  <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
               </field>
               <xsl:for-each select="distinct-values(//@xml:lang)">
                  <field name="language">
                     <xsl:value-of select="." />
                  </field>
               </xsl:for-each>
               <xsl:variable name="image_id" select="./@facs"/>
               <xsl:if test="./@facs">
                  <field name="image_id"> 
                     <xsl:value-of select="$image_id"/>
                  </field>
               <!-- give me image contains var id -->
               <xsl:variable name="clean_image_id" select="substring($image_id, 2)"/>
                  <field name="image_url">
                     <xsl:value-of select="img:get_path($clean_image_id, $context)"/>
                 </field> 
                  
               </xsl:if>
               
               <xsl:variable name="current-pb" select="."/>
               <xsl:variable name="next-pb" select="following::pb[1]"/>
               <xsl:choose>
                  <xsl:when test="$next-pb">
                     <xsl:for-each select="$current-pb/following::name intersect $next-pb/preceding::name">
                        <xsl:call-template name="make-entities-on-page-if-possible">
                           <xsl:with-param name="name" select="."/>
                        </xsl:call-template>
                     </xsl:for-each>
                  </xsl:when>
                  <!-- in this case we are on the last page of the letter -->
                  <xsl:otherwise>
                     <xsl:for-each select="$current-pb/following::name">
                        <xsl:call-template name="make-entities-on-page-if-possible">
                           <xsl:with-param name="name" select="."/>
                        </xsl:call-template>
                     </xsl:for-each>
                  </xsl:otherwise>
               </xsl:choose>
               
               <xsl:choose>
                  <xsl:when test="$next-pb">
                     <xsl:for-each select="$current-pb/following::note intersect $next-pb/preceding::note">
                        <xsl:call-template name="make-notes-on-page-if-possible">
                           <xsl:with-param name="note" select="."/>
                        </xsl:call-template>
                     </xsl:for-each>
                  </xsl:when>
                  <!-- in this case we are on the last page of the letter -->
                  <xsl:otherwise>
                     <xsl:for-each select="$current-pb/following::note">
                        <xsl:call-template name="make-notes-on-page-if-possible">
                           <xsl:with-param name="note" select="."/>
                        </xsl:call-template>
                     </xsl:for-each>
                  </xsl:otherwise>
               </xsl:choose>
               
            </doc>
         </xsl:if>
      </xsl:for-each-group>
   </xsl:template>

   <xsl:template name="page-beginning-with-possible-link">
      <xsl:param name="current-pb" />
      <div class="page-beginning">
         <xsl:variable name="facsId" select="substring($current-pb/@facs, 2, string-length($current-pb/@facs))" />
         <xsl:variable name="graphicUrl" select="id($facsId)/@url" />
         <xsl:variable name="graphicUrlWithoutJpg">
            <xsl:value-of select="if (ends-with($graphicUrl, '.jpg')) then substring($graphicUrl, 1, string-length($graphicUrl)-4) else $graphicUrl" />
         </xsl:variable>
         <xsl:choose>
            <xsl:when test="$current-pb/@n ne '' and $graphicUrlWithoutJpg">
               <a href="{concat('/', $graphicUrlWithoutJpg)}" target="_blank">
                  <xsl:value-of select="$current-pb/@n" />
               </a>
            </xsl:when>
            <xsl:when test="$current-pb/@n ne ''">
               <xsl:value-of select="$current-pb/@n" />
            </xsl:when>
            <xsl:otherwise>
               <xsl:text> </xsl:text>
            </xsl:otherwise>
         </xsl:choose>
      </div>
   </xsl:template>

   <xsl:template match="pb" mode="page_splitting" />

   <xsl:template match="*" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <xsl:message>
            <xsl:text>Unknown element &lt;</xsl:text>
            <xsl:value-of select="local-name()" />
            <xsl:if test="@rendition">
               <xsl:text> rendition="</xsl:text>
               <xsl:value-of select="@rendition" />
               <xsl:text>"</xsl:text>
            </xsl:if>
            <xsl:if test="@type">
               <xsl:text> type="</xsl:text>
               <xsl:value-of select="@type" />
               <xsl:text>"</xsl:text>
            </xsl:if>
            <xsl:text>&gt; - first occurrence: </xsl:text>
         </xsl:message>
         <span class="unknown-element">
            <xsl:apply-templates mode="page_splitting" />
         </span>
      </xsl:if>
   </xsl:template>

   <xsl:template match="body | div | front" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <xsl:apply-templates mode="page_splitting" />
      </xsl:if>
   </xsl:template>

   <xsl:template match="p | opener | salute | bibl | closer | signed | dateline | date | app | supplied
      | label[not(@rendition)] | choice | abbr | expan | orig | corr | addrLine | head | address | space | del | add | rdg | postscript" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="{local-name(.)}">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="text()" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <xsl:variable name="currentText" select="replace(., '\s+', ' ')" />
         <xsl:choose>
            <xsl:when test="ends-with(., '&#0173;')">
               <xsl:value-of select="replace($currentText, '&#0173;', '-')" />
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="$currentText" />
            </xsl:otherwise>
         </xsl:choose>
      </xsl:if>
   </xsl:template>

   <xsl:template match="lb" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <br />
      </xsl:if>
   </xsl:template>

   <xsl:template match="space[@unit='lines']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <xsl:variable name="emptyLines" select="@quantity" />
         <xsl:choose>
            <xsl:when test="$emptyLines castable as xs:integer">
               <xsl:for-each select="1 to $emptyLines">
                  <br />
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <br />
            </xsl:otherwise>
         </xsl:choose>
      </xsl:if>
   </xsl:template>

   <xsl:template match="name[@type='place']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="place">
            <xsl:attribute name="id"> 
               <xsl:value-of select="@ref" />
            </xsl:attribute>
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="name[@type='org']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="org">
            <xsl:attribute name="id"> 
               <xsl:value-of select="@ref" />
            </xsl:attribute>
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="name[@type='person']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="person" >
            <xsl:attribute name="id"> 
               <xsl:value-of select="@ref" />
            </xsl:attribute>
               <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="name[@type='object']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="object">
            <xsl:attribute name="id"> 
               <xsl:value-of select="@ref" />
            </xsl:attribute>
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="rs[@type='person']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="rs-person">
            <xsl:attribute name="id"> 
               <xsl:value-of select="@ref" />
            </xsl:attribute>
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="rs[@type='place']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="rs-place">
            <xsl:attribute name="id"> 
               <xsl:value-of select="@ref" />
            </xsl:attribute>
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="note[@type='com']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="note-comment">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="note[@type='footnote']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="note-footnote">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="note[not(@type)]" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="note">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="hi[@rendition='simple:centre']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="centre">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>
   
   <xsl:template match="hi[@rendition='simple:doubleunderline']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="doubleunderline">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="hi[@rendition='simple:underline']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="underline">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="hi[@rendition='simple:superscript']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <sup>
            <xsl:apply-templates mode="page_splitting" />
         </sup>
      </xsl:if>
   </xsl:template>

   <xsl:template match="hi[@rendition='simple:subscript']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <sub>
            <xsl:apply-templates mode="page_splitting" />
         </sub>
      </xsl:if>
   </xsl:template>

   <xsl:template match="hi[@rendition='simple:italic']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="italic">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="hi[@rendition='simple:letterspace']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="letterspace">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="hi[@rendition='simple:right']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="right">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>
   
   <xsl:template match="hi[@rendition='simple:smallcaps']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="smallcaps">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>
   
   <xsl:template match="hi[@rendition='simple:wavyunderline']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="wavyunderline">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>

   <xsl:template match="ref[@target]" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <a href="{@target}">
            <xsl:apply-templates mode="page_splitting" />
         </a>
      </xsl:if>
   </xsl:template>

   <xsl:template match="head[@rendition] | label[@rendition]" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <xsl:variable name="classNames" select="local-name(), substring-after(@rendition, 'simple:')" />
         <div class="{$classNames}">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>
   
   <xsl:template match="div[@rendition='simple:half-broken']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="half-broken">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>
   
   <xsl:template match="cb[@rendition='simple:column-left']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="column-left">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>
   
   <xsl:template match="cb[@rendition='simple:column-right']" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="column-right">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>
   
   <xsl:template match="date[@when]" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="date">
            <xsl:attribute name="id"> 
               <xsl:value-of select="@when" />
            </xsl:attribute>
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>
   
   <xsl:template match="sic" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="sic">
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>
   
   <xsl:template match="seg[@xml:id]" mode="page_splitting">
      <xsl:param name="restricted-to" tunnel="yes" />
      <xsl:if test="exists(. intersect $restricted-to)">
         <div class="seg">
            <xsl:attribute name="id"> 
               <xsl:value-of select="@xml:id" />
            </xsl:attribute>
            <xsl:apply-templates mode="page_splitting" />
         </div>
      </xsl:if>
   </xsl:template>
   
   <xsl:template name="make-entities-on-page-if-possible">
      <xsl:param name="name"/>
      <xsl:variable name="ref" select="$name/@ref"/>
      <xsl:if test="$ref != 'gnd:_' and starts-with($ref, 'gnd:') ">
         
         <field name="entities">
            <xsl:value-of select="$ref"/>
         </field>
      
      </xsl:if>
   </xsl:template>
   
   <xsl:template name="make-notes-on-page-if-possible">
      <xsl:param name="note"/>
      <field name="note_comment">
            <xsl:value-of select="$note/normalize-space()"/>
         </field>
   </xsl:template>

</xsl:stylesheet>