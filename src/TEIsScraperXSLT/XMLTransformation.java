package TEIsScraperXSLT;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * 
 * @author jerome
 *TODO die ordner auf ausserhalb des projekts
 */

public class XMLTransformation {
	public static void main(String[] args) throws IOException, TransformerConfigurationException {
		System.out.println(getPWD());
		System.out.println("remove old SOLR-XML files ...");
		removeOldSolrXMLFiles();
		System.out.println("Starte TEIs to SOLR-XML transformation ...");
		long startTime = System.currentTimeMillis();
		String xslFileName = "gfl-indexer.xslt";
		createDirIfNotExist();
		Transformer transformer = createTransformer(xslFileName);
		List<File> files = new ArrayList<File>();
		var path_without_pwd = getPWD().replaceFirst("tei-to-solrxml", "");
		listFiles(path_without_pwd+"volltexte/Lit/Lit_Fbl_Bln", files);// fill list files by sideeffect 	
		listFiles(path_without_pwd+"volltexte/TEI_doc_bearb/", files);	
		Integer count = 0;
		for (File file : files) {
			System.out.println(file.getName()+" wird transformiert");
			tEIStoSolrXMLwithXSLT(file.getPath(), transformer);
			count++;
		}
		long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    float seconds = elapsedTime/1000;
	    int minutes =  (int) (seconds/60);

		System.out.println(count+" Dateien erstellt in: " + minutes +" Minuten und "+(seconds%60)+" Sekunden");
	}
	/**
	 * factory method for transformer 
	 * @param xslFileName
	 * @return
	 * @throws TransformerConfigurationException
	 */
	public static Transformer createTransformer(String xslFileName) throws TransformerConfigurationException {		
		return TransformerFactory.newInstance().newTransformer(new StreamSource(xslFileName));
	} 
	
	/**
	 * 
	 * @param xmlFileName path to xml file
	 * @param xslFileName path to xslt
	 * @throws IOException 
	 */
	public static void tEIStoSolrXMLwithXSLT(String xmlFileName, Transformer transformer) throws IOException {
		String filename = "SOLR_"+cutNameFromPath(xmlFileName);
		StreamSource source = new StreamSource(xmlFileName);	
		StreamResult result = new StreamResult(new File(getPWD().replaceFirst("tei-to-solrxml", "")+"SolrXML/"+filename));
		try {
			transformer.transform(source, result);
			System.out.println("Datei erfolgreich erstellt: " + filename);
		} catch (TransformerException e) {
			System.out.println("Fehler beim Transformieren bei Datei: " + filename);
			e.printStackTrace();
		}
	}
	/**
	 * list all files inclusive subdirs
	 * @param dirName
	 * @param files
	 */
	public static void listFiles(String dirName, List<File> files) {
		File dir = new File(dirName);
		File[] fileList = dir.listFiles();
		if(fileList != null)
			for (File file : fileList) {      
				if (file.isFile()) {
					if(file.getName().endsWith(".xml")||file.getName().endsWith(".XML")) {
						files.add(file);
			        }
				} else if (file.isDirectory()) {
					listFiles(file.getAbsolutePath(), files);
				}
			}
	}
	
	/**
	 * 
	 * @return String working directory
	 * @throws IOException
	 */
	public static String getPWD() throws IOException {
		return new java.io.File( "." ).getCanonicalPath();
	}

	/**
	 * 
	 * @param path get the path of the xml file
	 * @return String name of the 
	 */
	public static String cutNameFromPath(String path) {
		String temp[] = path.split("/");
		return temp[temp.length-1];

	}
	/**
	 * check if there is a SOLR dir otherwise create one
	 * @throws IOException
	 */
	public static void createDirIfNotExist() throws IOException {
        String directoryPath = getPWD().replaceFirst("tei-to-solrxml/", "/") + "/SolrXML";// could be an agrument
        File file = new File(directoryPath);
 
        if (!file.exists()) {
        	file.mkdirs();
            System.out.println("Ordner "+file +" wurde erstellt");
        } else {
            System.out.println(file + " gibt es schon!");
        }
	}/**
	 * remove old xml files in solr xml dir
	 * @throws IOException
	 */
	public static void removeOldSolrXMLFiles() throws IOException {
		String directoryPath = getPWD().replaceFirst("tei-to-solrxml", "SolrXML");
        File dir = new File(directoryPath);
        if(dir.exists()) {
        for (File file : dir.listFiles()) {
        	if(file.delete())
            {
        		System.out.println(file +" file deleted successfully");
            }
            else
            {
                System.out.println("Failed to delete the file");
            }
        }
        }
	}
	
}//class
