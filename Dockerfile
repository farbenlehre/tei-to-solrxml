FROM openjdk:17
COPY . /TEItoSolrXML/
WORKDIR /TEItoSolrXML/
CMD ["java", "-jar", "TEIs2SolrXML.jar"]
